const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	devtool: 'source-map',
	entry: './app/main.ts',
	plugins: [
		new CleanWebpackPlugin({
			cleanAfterEveryBuildPatterns: ['public']
		}),
		new CopyPlugin([
			{ from: './app/img/*', to: 'data/img/', flatten: true },
			{ from: './app/css/*', to: 'data/css/', flatten: true },
		  ]),
		new HtmlWebpackPlugin({
			template: './app/index.html',
			title : 'Intertwined Map'
		}),
	],
	output: {
		path: __dirname + '/public',
		filename: '[name].js'
	},
	resolve: {
		extensions: ['.css', '.js', '.svg', '.ts', '.tsx']
	},
	module: {
		rules: [
			{ test: /\.tsx?$/, use: 'ts-loader' },
			{ test: /\.css$/, use: ['style-loader', 'css-loader'] },
			{ test: /\.svg$/, use: [
				{
				  loader: 'file-loader',
				},
			  ]
			}
		]
	}
}