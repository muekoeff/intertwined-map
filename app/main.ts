import $ from 'jquery';
import { GeoJsonObject } from 'geojson';
import osmtogeojson from 'osmtogeojson';
import L from 'leaflet';
import "leaflet-hash";
import Util from './js/util';
import { Bbox, DataSourceMeta, RowDefinition, ImagePreloader, SourceOsm, SourceWikidata, WikidataId, WikidataIdSource, WikidataProperty } from './js/intertwinedMap';

var bbox: string;
var bbox_overpass: Bbox = null;
var imagepreload_requests: ImagePreloader[] = [];
var layer_group: L.LayerGroup;
var map: L.Map;
var nameAlternatives = ["name", "brand", "ref", "operator"];
var osmkey_wikidataid_pairs: [string, WikidataId][];
var overpass_request: JQueryXHR = null;
// @TODO: Add website
var properties = [
	new WikidataProperty("P18", "image", ["wikidata"]),
	new WikidataProperty("P84", "architect", ["wikidata"]),
	new WikidataProperty("P137", "operator", ["wikidata"]),
	new WikidataProperty("P154", "logo_image", ["wikidata", "brand:wikidata", "operator:wikidata"]),
	new WikidataProperty("P791", "ref:isil", ["wikidata"]),
	new WikidataProperty("P856", "contact:website", ["wikidata"]),
	new WikidataProperty("P1813", "short_name", ["wikidata"]),
	new WikidataProperty("P2627", "bic", ["brand:wikidata", "operator:wikidata"])
];
var wikicommons_requests: JQueryXHR[] = [];
var wd_id_sources = [
	new WikidataIdSource("wikidata", "darkorchid"),
	new WikidataIdSource("operator:wikidata", "orangered"),
	new WikidataIdSource("brand:wikidata", "mediumseagreen"),
	new WikidataIdSource("subject:wikidata", "goldenrod")
];
var wd_requests: JQueryXHR[] = [];

function setupMap() {
	// Create map element
	map = L.map('map').setView([51, 10], 6);
	layer_group = L.layerGroup().addTo(map);
	// Add OSM as layer
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
		maxNativeZoom: 19,
		maxZoom: 21
	}).addTo(map);
	// Add scale
	L.control.scale().addTo(map);
	// Enable hash
	var hash = new (<any>L).Hash(map);
	// Add custom controls to map
	var customNavControls = function (opts: L.ControlOptions) {
		var CustomNavControls = L.Control.extend({
			onAdd: function (map: L.Map) {
				// Search control
				var searchControlIcon = createIconRow("Search for a place", "button", "data/img/Search-WF.svg");
				searchControlIcon.onclick = function (e) {
					e.preventDefault()
					var $textfield = $("#leaflet-control-search-input");
					if ($textfield.val() === "" || $textfield.val() === $textfield.attr("placeholder")) {
						$textfield.val($textfield.attr("placeholder"));
					}
					$textfield.focus().select();
				}
				expandableLabelRow(searchControlIcon, searchControlIcon.getAttribute("title"));
				var searchControlField = L.DomUtil.create('input');
				searchControlField.id = "leaflet-control-search-input";
				searchControlField.setAttribute("placeholder", "Look for a place...");
				searchControlField.setAttribute("type", "text");
				searchControlField.onblur = function (e) {
					$(this).closest(".leaflet-control-expandable").removeClass("open");
				}
				searchControlField.onfocus = function (e) {
					$(this).closest(".leaflet-control-expandable").addClass("open");
				}
				searchControlField.onkeyup = function (e) {
					if (e.key == "Enter") {
						uiPageSet("page-geocoding");
						$("#geocoding-query").val($(this).val()).attr("readonly", "true");
						nominatim(String($("#geocoding-query").val()));
					}
				}
				var searchControlWrapper = L.DomUtil.create('div', 'leaflet-control-expandable-row');
				searchControlWrapper.appendChild(searchControlIcon);
				searchControlWrapper.appendChild(searchControlField);
				var searchControlContainer = L.DomUtil.create('div', 'leaflet-control-growth');
				searchControlContainer.appendChild(searchControlWrapper);
				// Locate control
				var locateControl = createIconRow("Locate", "button", "data/img/Location-01-WF.svg");
				locateControl.id = "control_locate";
				locateControl.onclick = function (e) {
					e.preventDefault();
					map.on("locationerror", function (e: L.ErrorEvent) {
						console.debug(e);
						$("#control_locate").addClass("blocked");
						uiErrorAdd("location", `Failed to aquire location: ${e.message}`)
					});
					map.on("locationfound", function (e: L.LocationEvent) {
						console.debug(e);
						$("#control_locate").removeClass("blocked");
						uiErrorRemove("location")
					});
					map.locate({
						setView: true
					});
				};
				// Download toggle control
				var downloadToggleControl = createIconRow("Toggle download", "checkbox", "data/img/Download-02-WF.svg");
				downloadToggleControl.id = "control_downloadtoggle";
				downloadToggleControl.onclick = function (e) {
					e.preventDefault();
					if ($(this).hasClass("leaflet-control-disabled")) {
						$(this).removeClass("leaflet-control-disabled");
						overpassRequest();
					} else {
						$(this).addClass("leaflet-control-disabled");
						overpassCancel();
					}
				};
				// Visibility toggle control
				var visibilityToggleControl = createIconRow("Toggle visibility", "checkbox", "data/img/Show-02-WF.svg");
				visibilityToggleControl.onclick = function (e) {
					e.preventDefault();
				};
				// Settings control
				var settingsControl = createIconRow("Open settings", "button", "data/img/Settings-WF.svg");
				settingsControl.onclick = function (e) {
					e.preventDefault();
					if ($("#page-settings").hasClass("visible")) {
						uiPageSet("page-tags");
					} else {
						uiPageSet("page-settings");
					}
				};
				// Create group
				var control_container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-expandable');
				control_container.appendChild(searchControlWrapper);
				control_container.appendChild(expandableLabelRow(locateControl, locateControl.getAttribute("title")));
				control_container.appendChild(expandableLabelRow(downloadToggleControl, downloadToggleControl.getAttribute("title")));
				control_container.appendChild(expandableLabelRow(visibilityToggleControl, visibilityToggleControl.getAttribute("title")));
				control_container.appendChild(expandableLabelRow(settingsControl, settingsControl.getAttribute("title")));
				return control_container;
			},
			onRemove: function (map: L.Map) {
				// Nothing to do here
			}
		});
		return new CustomNavControls(opts);
	}
	var customExtControls = function (opts: L.ControlOptions) {
		var CustomExtControls = L.Control.extend({
			onAdd: function (map: L.Map) {
				// Osm control
				var osmControl = createIconRow("Open current view in OpenStreetMap", "button", "data/img/Openstreetmap.svg");
				osmControl.onmouseenter = function (e) {
					$(osmControl).attr("href", `https://osm.org/#map=${map.getZoom()}/${map.getCenter().lat}/${map.getCenter().lng}`);
				};
				// Overpass control
				var overpassTurboControl = createIconRow("Open current view in Overpass Turbo", "button", "https://wiki.openstreetmap.org/w/images/c/c3/Overpass-turbo.svg");
				overpassTurboControl.onmouseenter = function (e) {
					$(overpassTurboControl).attr("href", `https://overpass-turbo.eu/?C=${map.getCenter().lat};${map.getCenter().lng};${map.getZoom()}`);
				};
				// Create group
				var control_container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-expandable');
				control_container.appendChild(expandableLabelRow(osmControl, osmControl.getAttribute("title")));
				control_container.appendChild(expandableLabelRow(overpassTurboControl, overpassTurboControl.getAttribute("title")));

				return control_container;
			},
			onRemove: function (map: L.Map) {
				// Nothing to do here
			}
		});
		return new CustomExtControls(opts);
	}
	customNavControls({
		position: "topleft"
	}).addTo(map);
	customExtControls({
		position: "topleft"
	}).addTo(map);
	// Add event listener for Overpass
	map.on("moveend", function () {
		overpassRequest();
	});

	function createIconRow(title: string, role: string, icon: string) {
		var newItem = L.DomUtil.create('a', "leaflet-control-image");
		newItem.setAttribute("aria-label", title);
		newItem.setAttribute("href", "#");
		newItem.setAttribute("role", role);
		newItem.setAttribute("title", title);
		newItem.innerHTML = `<span style="background-image:url(${icon})"></span>`;
		return newItem;
	}

	function wrapInExpandableRow(left: HTMLElement, right: HTMLElement): HTMLElement {
		var expandableRow = L.DomUtil.create("div", "leaflet-control-expandable-row");
		expandableRow.appendChild(left);
		expandableRow.appendChild(right);
		return expandableRow;
	}

	function expandableLabelRow(left: HTMLElement, label: string): HTMLElement {
		var expandableLabel = L.DomUtil.create("span", "leaflet-control-expandable-label");
		expandableLabel.innerHTML = label;
		return wrapInExpandableRow(left, expandableLabel);
	}
}
function resetState() {
	// Cancel ongoing requests and reset state
	imagePreloadCancel();
	wikidataCancel();
	wikicommonsCancel();
	// Reset UI
	$("#item-logo").removeAttr("data-hierarchy").removeClass("loaded").html("");
	$("#item-header-top").removeAttr("data-hierarchy").removeClass("image");
	$("#item-header-top-image").css("background-image", "");
	// Clear data
	osmkey_wikidataid_pairs = [];
}
function updateItemBar(layer: L.Layer) {
	resetState();

	$("#item-header h2 span").html(getName());
	$("#item-tags").html(getTable());
	wikidataRequest(osmkey_wikidataid_pairs);

	function getName() {
		let out_name;
		for (let i = 0; i < nameAlternatives.length && out_name == null; i++) {
			let item = (<any> layer).feature.properties[nameAlternatives[i]];
			if (item !== undefined) {
				out_name = Util.sanitize(item);
				if (nameAlternatives[i] !== "name") out_name += ` <small>(${nameAlternatives[i]})</small>`;
			}
		}
		// @TODO: Hardcoded string
		return out_name != null ? out_name : "Namenlos";
	}

	function getTable() {
		let out = "<table>";
		$.each((<any> layer).feature.properties, function (index, value) {
			out += generateTableRow(new SourceOsm((<any> layer).feature.properties.id, (<any> layer).feature.properties.type), String(index), value);
		});
		out += "</table>";
		return out;
	}
}
function generateTableRow(source: DataSourceMeta, key: string, value: string) {
	return `<tr data-osmkey="${key}" data-hierarchy="${source.getHierarchy()}">
				<td><a href="${source.getUrl()}" rel="noopener noreferrer" title="${source.getTitle()}">${source.getIcon()}</a></td>
				<td>${Util.sanitize(key)}</td>
				<td>${linkExternal(key, value)}</td>
			</tr>`;

	function linkExternal(key: string, value: string) {
		// Wikidata
		if ((key == "wikidata" || key.endsWith(":wikidata")) && value.startsWith("Q")) {
			// As a side product, store wikidata id
			osmkey_wikidataid_pairs.push([value, key]);
			return `<a href="http://www.wikidata.org/entity/${Util.sanitize(value)}" rel="noopener noreferrer">${Util.sanitize(value)}</a> <img height="16" src="https://upload.wikimedia.org/wikipedia/commons/f/ff/Wikidata-logo.svg" />`;
		} else if ((key == "wikipedia" || key.endsWith(":wikipedia")) && value.includes(":")) {
			return `<a href="https://en.wikipedia.org/wiki/${Util.sanitize(value)}" rel="noopener noreferrer">${Util.sanitize(value)}</a> <img height="16" src="https://upload.wikimedia.org/wikipedia/commons/8/80/Wikipedia-logo-v2.svg" />`;
		} else if ((key == "website" || key.endsWith(":website")) && (value.startsWith("http://") || value.startsWith("https://"))) {
			return `<a href="${Util.sanitize(value)}" rel="noopener noreferrer" target="_blank">${Util.sanitize(value)}</a>`;
		} else {
			return Util.sanitize(value);
		}
	}
}
function imagePreloadCancel() {
	if (imagepreload_requests !== undefined && imagepreload_requests !== null) {
		imagepreload_requests.forEach(function (request, index) {
			request.cancel();
		});
		imagepreload_requests = [];
		console.debug("Image preloader requests cancelled");
	}
}
function mapClearMarker(layer_group: L.LayerGroup) {
	if (layer_group !== undefined && layer_group !== null) {
		layer_group.clearLayers();
	}
}
function mapElements(data: any) {
	mapClearMarker(layer_group);

	var geoJson = <unknown> osmtogeojson(data);
	L.geoJSON(<GeoJsonObject>geoJson, {
		style: function (feature: GeoJSON.Feature<GeoJSON.GeometryCollection, any>) {
			let color = null;
			for (var i = 0; i < wd_id_sources.length && color == null; i++) {
				let wd_id_source = wd_id_sources[i];
				if (feature.properties[wd_id_source.osmKey] !== undefined) {
					color = wd_id_source.color;
				}
			}
			return { color: color == null ? "slategray" : color };
		},
		onEachFeature: function (feature: GeoJSON.Feature<GeoJSON.GeometryObject, any>, layer: L.Layer) {
			layer.addTo(layer_group);
			layer.on("click", function (e) {
				uiPageSet("page-tags");
				updateItemBar(layer);
			});
		},
		pointToLayer: function (geoJsonPoint: GeoJSON.Feature<GeoJSON.Point, any>, latlng: L.LatLng) {
			return L.circleMarker(latlng, {
				radius: 6
			});
		}
	}).addTo(map);
}
function nominatim(query: string) {
	var request = $.ajax({
		url: `https://nominatim.openstreetmap.org/search.php?q=Saarland&polygon_geojson=1&viewbox=`,
		data: {
			"format": "jsonv2",
			"q": query,
			"polygon_geojson": "1"
		},
		beforeSend: function (jqXHR, settings) {
			uiLoadingAdd("nominatim", "Loading search results from Nominatim&hellip;");
		}
	}).always(function (data, textStatus, jqXHR) {
		uiLoadingRemove("nominatim");
	}).fail(function (jqXHR, textStatus, errorThrown) {
		if (errorThrown !== "abort") {
			uiErrorAdd("nominatim", `Failed to load details from Nominatim: ${errorThrown}`);
			console.warn(jqXHR, textStatus, errorThrown);
		}
	}).done(function (data, textStatus, jqXHR) {
		console.debug(data);
		$("#geocoding-results").html("");
		data.forEach(function (item: any, index: number) {
			$("#geocoding-results").append(generateItem(item));
		});
	});

	function generateItem(item: any) {
		let $wrapper = $(`<div>
				<a href="#select">
					${item.icon !== undefined && item.icon !== null ? `<img src="${item.icon}" alt="${item.type}" />` : ""}
					${Util.sanitize(item.display_name)}
				</a>
				(<a href="https://osm.org/${item.osm_type}/${item.osm_id}">OSM</a>)
			</div>`);
		$wrapper.find("a[href='#select']").click(function (e) {
			e.preventDefault();
			map.fitBounds([[item.boundingbox[0], item.boundingbox[2]],
			[item.boundingbox[1], item.boundingbox[3]]]);
		});
		return $wrapper;
	}
}
function overpassCancel() {
	if (overpass_request !== undefined && overpass_request !== null) {
		overpass_request.abort();
		console.log("Overpass request cancelled");
	}
}
function overpassRequest() {
	// Check, if download enabled
	if ($("#control_downloadtoggle").hasClass("leaflet-control-disabled")) {
		return;
	}
	// Check, if zoom great enough
	if (map.getZoom() < 12) {
		console.debug("Not zoomed in far enough");
		$("#control_downloadtoggle").addClass("blocked");
		return;
	} else {
		$("#control_downloadtoggle").removeClass("blocked");
	}
	// Check, if user has zoomed in - therefore not requiring a new request
	if (bbox_overpass != null && bbox_overpass.south < map.getBounds().getSouth() && bbox_overpass.west < map.getBounds().getWest() && bbox_overpass.north > map.getBounds().getNorth() && bbox_overpass.east > map.getBounds().getEast()) {
		console.debug("Current bbox is completely in last requested bbox");
		return;
	}
	// Cancel ongoing previous request, if necessary
	overpassCancel();
	// Store bbox
	bbox_overpass = new Bbox(map.getBounds().getSouth(), map.getBounds().getWest(), map.getBounds().getNorth(), map.getBounds().getEast());
	bbox = `${map.getBounds().getSouth()},${map.getBounds().getWest()},${map.getBounds().getNorth()},${map.getBounds().getEast()}`;
	// Generate rules
	let rules = "";
	wd_id_sources.forEach(function (wd_id_source, index) {
		rules += `node["wikidata"];
		way["wikidata"];
		rel["wikidata"]["type"="multipolygon"];
		node[~":wikidata"~".*"];
		way[~":wikidata"~".*"];
		rel[~":wikidata"~".*"]["type"="multipolygon"];`;
	});
	// Request
	overpass_request = $.ajax({
		url: "https://overpass-api.de/api/interpreter",
		data: {
			"data": `[out:json][timeout:25][bbox:${bbox}];
			(${rules});
			out body;
			>;
			out skel qt;`.replace("\n", "").replace("\t", "")
		},
		beforeSend: function (jqXHR, settings) {
			uiLoadingAdd("overpass", "Loading elements from Overpass&hellip;");
			$("#control_downloadtoggle").addClass("working");
		}
	}).always(function (data, textStatus, jqXHR) {
		overpass_request = null;
		uiLoadingRemove("overpass");
		$("#control_downloadtoggle").removeClass("working");
	}).fail(function (jqXHR, textStatus, errorThrown) {
		if (errorThrown !== "abort") {
			uiErrorAdd("overpass", `Failed to load elements from Overpass: ${errorThrown}`);
			console.warn(jqXHR, textStatus, errorThrown);
		}
	}).done(function (data, textStatus, jqXHR) {
		mapElements(data);
	});
}
export function uiErrorAdd(identifier: string, message: string, timeout?: number) {
	timeout = timeout == undefined ? 10000 : timeout;
	if ($(`#error-${identifier}`).length == 0) {
		$("#errormsg-wrapper ul").append(`<li id="error-${identifier}">${message}</li>`);
	} else {
		$(`#error-${identifier}`).html(message);
	}
	if (timeout != null && timeout != 0) {
		setTimeout(function () {
			uiErrorRemove(identifier);
		}, timeout);
	}
	$("#errormsg-wrapper").addClass("visible");
}
export function uiErrorRemove(identifier: string) {
	if ($(`#error-${identifier}`).length == 0) {
		return false;
	} else {
		$(`#error-${identifier}`).remove();
		if ($("#errormsg-wrapper ul").children().length == 0) {
			$("#errormsg-wrapper").removeClass("visible");
		}
		return true;
	}
}
export function uiLoadingAdd(identifier: string, message: string) {
	if ($(`#loading-${identifier}`).length == 0) {
		$("#item-loadingindicator").append(`<span id="loading-${identifier}">${message}</span>`);
	} else {
		$(`#loading-${identifier}`).html(message);
	}
	$("#item-header").addClass("loading");
}
export function uiLoadingRemove(identifier: string) {
	if ($(`#loading-${identifier}`).length == 0) {
		return false;
	} else {
		$(`#loading-${identifier}`).remove();
		if ($("#item-loadingindicator").children().length == 0) {
			$("#item-header").removeClass("loading");
		}
		return true;
	}
}
export function uiPageSet(identifier: string) {
	$(".aside-page").each(function (index, el) {
		if ($(el).attr("id") == identifier) {
			$(el).addClass("visible");
		} else {
			$(el).removeClass("visible");
		}
	});
}
function wikicommonsCancel() {
	if (wikicommons_requests !== undefined && wikicommons_requests !== null) {
		wikicommons_requests.forEach(function (request, index) {
			request.abort();
		});
		wikicommons_requests = [];
		console.debug("Wikicommons requests cancelled");
	}
}
function wikicommonsGetFullPathFromName(name: string, callback: Function) {
	var request = $.ajax({
		url: `https://commons.wikimedia.org/w/api.php`,
		data: {
			"action": "query",
			"format": "json",
			"origin": "*",
			"prop": "imageinfo",
			"iiprop": "extmetadata|url",
			"titles": `File:${name}`
		},
		beforeSend: function (jqXHR, settings) {
			uiLoadingAdd("wikicommons-meta", "Loading meta data from Wikimedia Commons&hellip;");
		}
	}).always(function (data, textStatus, jqXHR) {
		wikicommons_requests = Util.removeFromArray(request, wikicommons_requests);
		uiLoadingRemove("wikicommons-meta");
	}).fail(function (jqXHR, textStatus, errorThrown) {
		if (errorThrown !== "abort") {
			uiErrorAdd("wikicommons-meta", `Failed to load meta data from Wikimedia Commons: ${errorThrown}`);
			console.warn(jqXHR, textStatus, errorThrown);
		}
	}).done(function (data, textStatus, jqXHR) {
		callback(data);
	});
	wikicommons_requests.push(request);
}
function wikidataCancel() {
	if (wd_requests !== undefined && wd_requests !== null) {
		wd_requests.forEach(function (request, index) {
			request.abort();
		});
		wd_requests = [];
		console.debug("Wikidata requests cancelled");
	}
}
function wikidataFilter(ids: [WikidataId, string][], data: any) {
	let out: RowDefinition[] = [];
	if (data.success == 1) {
		$.each(data.entities, function (wdId: WikidataId, value) {
			let osmKey = ids.find(function (e: [WikidataId, string]) {
				return e[0] == wdId;
			});

			extractProperties(osmKey[1], wdId, value).forEach(function (value, index) {
				out.push(value);
			});
		});
	}
	return out;

	function extractProperties(osmKey: string, wdId: WikidataId, entity: any) {
		let out: RowDefinition[] = [];
		properties.forEach(function (wdProperty, index) {
			if (entity.claims[wdProperty.propertyId] !== undefined) {
				out.push(flatten(osmKey, wdId, wdProperty, entity.claims[wdProperty.propertyId]));
			}
		});
		return out;
	}

	function flatten(osmKey: string, wdId: WikidataId, wdProperty: WikidataProperty, claims: any[]) {
		if (claims.length == 0) {
			return null;
		} else if (claims.length == 1) {
			return filterRelevant(osmKey, wdId, wdProperty, claims[0].mainsnak);
		} else {
			// @TODO: Find best claim
			return filterRelevant(osmKey, wdId, wdProperty, claims[0].mainsnak);
		}

		function filterRelevant(osmKey: string, wdId: WikidataId, wdProperty: WikidataProperty, mainsnak: any) {
			switch (mainsnak.datatype) {
				case "commonsMedia":
				case "external-id":
				case "string":
				case "url":
					return new RowDefinition(osmKey, wdId, wdProperty, mainsnak.datavalue.value);
				case "monolingualtext":
					// @TODO: Filter language
					return new RowDefinition(osmKey, wdId, wdProperty, mainsnak.datavalue.value.text);
				case "wikibase-item":
					// @TODO: List with prefixed wikidata
					return new RowDefinition(osmKey, wdId, wdProperty, mainsnak.datavalue.value.id);
				default:
					console.warn(`Unknown datatype ${mainsnak.datatype}`, mainsnak);
					return null;
			}
		}
	}
}
function wikidataInsert(claims: RowDefinition[]) {
	var rows = $("#item-tags table tbody tr");

	claims.forEach(function (claim, index) {
		if (claim !== null) {
			var source = new SourceWikidata(claim.osmKey, claim.wdId, claim.wdProperty);
			if (source.getHierarchy() >= 0 && source.getHierarchy()) {
				switch (claim.wdProperty.osmKey) {
					case "image":
						let hierarchy_image = $("#item-header-top").attr("data-hierarchy");
						if (hierarchy_image == null || source.getHierarchy() > Number(hierarchy_image)) {
							setImage(claim, source);
						} else {
							console.debug(`Claim for ${claim.wdProperty.osmKey} has lower hierarchy than currently displayed one`);
						}
						break;
					case "logo_image":
						let hierarchy_logo_image = $("#item-logo").attr("data-hierarchy");
						if (hierarchy_logo_image == null || source.getHierarchy() > Number(hierarchy_logo_image)) {
							setLogo(claim, source);
						} else {
							console.debug(`Claim for ${claim.wdProperty.osmKey} has lower hierarchy than currently displayed one`);
						}
						break;
					default:
						let row = $("#item-tags").find(`tr[data-osmkey="${claim.wdProperty.osmKey}"]`);
						let hierarchy_row = row == null ? null : row.attr("data-hierarchy");
						if (hierarchy_row == null || source.getHierarchy() > Number(hierarchy_row)) {
							addRow(claim, source);
						} else {
							console.debug(`Claim for ${claim.wdProperty.osmKey} has lower hierarchy than currently displayed one`);
						}
						break;
				}
			}
		}
	});

	// @TODO: Add "updateRow"
	function addRow(claim: RowDefinition, source: SourceWikidata) {
		var n = 0;
		var exit = false;
		while (!exit) {
			if (n >= rows.length) {
				let toInsert = generateTableRow(source, claim.wdProperty.osmKey, claim.value);
				$(`#item-tags table tbody tr:nth-child(${n})`).after(toInsert);
				exit = true;
			} else if ((<HTMLElement>rows[n].children[1]).innerText.localeCompare(claim.wdProperty.osmKey) == -1) {
				n++;
				exit = false;
			} else {
				let toInsert = generateTableRow(source, claim.wdProperty.osmKey, claim.value);
				$(`#item-tags table tbody tr:nth-child(${n + 1})`).before(toInsert);
				exit = true;
			}
		}
	}
	function setImage(claim: RowDefinition, source: SourceWikidata) {
		wikicommonsGetFullPathFromName(claim.value, function (data: any) {
			let key1 = Object.keys(data.query.pages)[0];
			let key2 = Object.keys(data.query.pages[key1].imageinfo)[0];
			let imagepath = data.query.pages[key1].imageinfo[key2].url;
			let descriptionpath = data.query.pages[key1].imageinfo[key2].descriptionurl;

			uiLoadingAdd("wikicommons-preload-header", "Loading header image from Wikimedia Commons&hellip;");
			// Preload image
			new ImagePreloader(imagepreload_requests, imagepath, function () {
				$("#item-header-top-image").css("background-image", `url(${imagepath})`);
				$("#item-header-top").addClass("image").attr("data-hierarchy", source.getHierarchy());
			}, null, function () {
				uiLoadingRemove("wikicommons-preload-header");
			});
		});
	}
	function setLogo(claim: RowDefinition, source: SourceWikidata) {
		wikicommonsGetFullPathFromName(claim.value, function (data: any) {
			let key1 = Object.keys(data.query.pages)[0];
			let key2 = Object.keys(data.query.pages[key1].imageinfo)[0];
			let imagepath = data.query.pages[key1].imageinfo[key2].url;
			let descriptionpath = data.query.pages[key1].imageinfo[key2].descriptionurl;

			uiLoadingAdd("wikicommons-preload-logo", "Loading logo from Wikimedia Commons&hellip;");
			// Preload image
			new ImagePreloader(imagepreload_requests, imagepath, function () {
				$("#item-logo").html(`<a href="${descriptionpath}"><img class="${imagepath.endsWith(".svg") ? "svg " : ""}" src="${imagepath}" title="${source.getTitle()}" /></a>`).addClass("loaded").attr("data-hierarchy", source.getHierarchy());
			}, null, function () {
				uiLoadingRemove("wikicommons-preload-logo");
			})
		});
	}
}
function wikidataRequest(ids: [WikidataId, string][]) {
	var id_list: WikidataId[] = [];
	ids.forEach(function (value, index) {
		id_list.push(value[0]);
	});
	var request = $.ajax({
		url: `https://www.wikidata.org/w/api.php`,
		data: {
			"action": "wbgetentities",
			"format": "json",
			"languages": "en",
			"origin": "*",
			"ids": id_list.join("|")
		},
		beforeSend: function (jqXHR, settings) {
			uiLoadingAdd("wikidata", "Loading details for this element from Wikidata&hellip;");
		}
	}).always(function (data, textStatus, jqXHR) {
		wd_requests = Util.removeFromArray(request, wd_requests);
		uiLoadingRemove("wikidata");
	}).fail(function (jqXHR, textStatus, errorThrown) {
		if (errorThrown !== "abort") {
			uiErrorAdd("wikidata", `Failed to load details from Wikidata: ${errorThrown}`);
			console.warn(jqXHR, textStatus, errorThrown);
		}
	}).done(function (data, textStatus, jqXHR) {
		var claims = wikidataFilter(ids, data);
		wikidataInsert(claims);
	});
	wd_requests.push(request);
}
setupMap();
// Initial map markers
overpassRequest();