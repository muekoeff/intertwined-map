import Util from "./util";

export class Bbox {
	east: number;
	north: number;
	south: number;
	west: number;
	constructor(south: number, west: number, north: number, east: number) {
		this.east = east;
		this.north = north;
		this.south = south;
		this.west = west;
	}
}
export interface DataSourceMeta {
	getHierarchy(): number;
	getIcon(): string;
	getTitle(): string;
	getUrl(): string;
}
export class ImagePreloader {
	onerror: Function;
	onalways: Function;
	preloader: HTMLImageElement;

	constructor(manager: ImagePreloader[], src: string, onload: Function, onerror: Function, onalways: Function) {
		let _this = this;

		this.onalways = onalways;
		this.onerror = onerror;
		this.preloader = new Image();

		manager.push(this);

		this.preloader.src = src;
		this.preloader.onload = function () {
			manager = Util.removeFromArray(_this.preloader, manager);
			if (onalways !== null) onalways();
			if (onload !== null) onload();
		};
		this.preloader.onerror = function () {
			manager = Util.removeFromArray(_this.preloader, manager);
			if (onalways !== null) onalways();
			if (onerror !== null) onerror();
		};
	}

	cancel() {
		this.preloader.onload = function () { }
		if (this.onalways !== null) this.onalways();
		if (this.onerror !== null) this.onerror();
	}
}
export class SourceOsm implements DataSourceMeta {
	entityId: WikidataId;
	entityType: OsmType;

	constructor(entityId: WikidataId, entityType: OsmType) {
		this.entityId = entityId;
		this.entityType = entityType;
	}
	getHierarchy(): number {
		return 0;
	}
	getIcon(): string {
		return `<img height="16" src="https://upload.wikimedia.org/wikipedia/commons/b/b0/Openstreetmap_logo.svg" />`;
	}
	getTitle(): string {
		return `${this.entityType}/${this.entityId}`;
	}
	getUrl(): string {
		return `https://osm.org/${this.entityType}/${this.entityId}`;
	}
}
export class SourceWikidata implements DataSourceMeta {
	wdId: WikidataId;
	sourceOsmKey: string;
	wdProperty: WikidataProperty;

	constructor(sourceOsmKey: string, wdId: WikidataId, wdProperty: WikidataProperty) {
		this.sourceOsmKey = sourceOsmKey;
		this.wdId = wdId;
		this.wdProperty = wdProperty;
	}
	getHierarchy(): number {
		var osmKeyIndex = this.wdProperty.inclusiveScopes.indexOf(this.sourceOsmKey);
		if (osmKeyIndex == -1) {
			if (this.wdProperty.inclusiveScopes.indexOf("*") == -1) {
				return -1
			} else {
				return this.wdProperty.inclusiveScopes.length + 1;
			}
		} else {
			return osmKeyIndex + 1;
		}
	}
	getIcon(): string {
		return `<img height="14" src="https://upload.wikimedia.org/wikipedia/commons/f/ff/Wikidata-logo.svg" />`;
	}
	getTitle(): string {
		return `${this.sourceOsmKey} &mdash; ${this.wdId}#${this.wdProperty.propertyId}`;
	}
	getUrl(): string {
		return `http://www.wikidata.org/entity/${this.wdId}${this.wdProperty != null ? `#${this.wdProperty.propertyId}` : ""}`;
	}
}
export enum OsmType {
	Line,
	Relation,
	Node
}
export class RowDefinition {
	osmKey: string
	wdId: WikidataId
	wdProperty: WikidataProperty
	value: string

	constructor(osmKey: string, wdId: WikidataId, wdProperty: WikidataProperty, value: string) {
		this.osmKey = osmKey;
		this.wdId = wdId;
		this.wdProperty = wdProperty;
		this.value = value;
	}
}
export type WikidataId = string;
export class WikidataIdSource {
	color: string;
	osmKey: string;

	constructor(osmKey: string, color: string) {
		this.osmKey = osmKey;
		this.color = color;
	}
}
export class WikidataProperty {
	propertyId: WikidataId;
	osmKey: string;
	inclusiveScopes: string[];

	constructor(propertyId: WikidataId, osmKey: string, inclusiveScopes: string[]) {
		this.propertyId = propertyId;
		this.osmKey = osmKey;
		this.inclusiveScopes = inclusiveScopes;
	}
}